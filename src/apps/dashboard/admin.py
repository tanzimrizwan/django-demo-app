from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django.db import models
from .models import Customer, ServiceInfo

# Register your models here.


class CustomAdmin(admin.ModelAdmin):
    list_display = ["id", "username", "email"]
    list_filter = ["created_at"]
    search_fields = ["username"]

    class Meta:
        model = Customer


admin.site.register(Customer, CustomAdmin)


class ServiceInfoAdmin(admin.ModelAdmin):
    list_display = ["id", "user", "package_name"]
    list_filter = ["service_start_date"]
    search_fields = ["user"]

    class Meta:
        model = ServiceInfo


admin.site.register(ServiceInfo, ServiceInfoAdmin)