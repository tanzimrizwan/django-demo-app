from django.db import models

# Create your models here.

GENDER_CHOICES = (
    (0, 'male'),
    (1, 'female'),
    (2, 'not specified'),
)


class Customer(models.Model):
    username = models.CharField(max_length=15)
    fullname = models.CharField(max_length=30)
    email = models.EmailField(max_length=254)
    phone = models.CharField(max_length=30, null=True, blank=True)
    address = models.CharField(max_length=150)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=2)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.username


class ServiceInfo(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    package_name = models.CharField(max_length=30)
    package_price = models.DecimalField(max_digits=5, decimal_places=2)
    service_start_date = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.package_name
