from django.urls import path
from .views import CustomerListAPIView, CustomerCreateAPIView, CustomerServiceDetailAPIView

urlpatterns = [
    path('v1/customers/', CustomerListAPIView.as_view(), name='api_customers'),
    path('v1/customer/create/', CustomerCreateAPIView.as_view(), name='api_customer_create'),
    path('v1/customer/<int:pk>/', CustomerServiceDetailAPIView.as_view(), name='api_customer_detail'),
]
