from rest_framework import serializers
from ..models import Customer, ServiceInfo


class CustomerSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True)
    modified_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Customer
        fields = [
            'id',
            'username',
            'fullname',
            'email',
            'phone',
            'address',
            'gender',
            'created_at',
            'modified_at'
        ]


class CustomerServiceSerializer(serializers.ModelSerializer):
    packages = serializers.SerializerMethodField()

    def get_packages(self, obj):
        pkgs = []
        for i in ServiceInfo.objects.filter(user=obj.id):
            pkgs.append(i.package_name)
        return pkgs

    class Meta:
        model = Customer
        fields = [
            'username',
            'email',
            'phone',
            'gender',
            'address',
            'packages'
        ]
